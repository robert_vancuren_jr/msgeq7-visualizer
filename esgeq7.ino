//#define DEBUG true; 

#include <Adafruit_NeoPixel.h>

int numStrips = 7;
int ledDataPin = 8;
int ledsPerChannel = 5;
int numLeds = ledsPerChannel * numStrips;

double divideBy1023 = 1.0 / 1023.0;
double divideTimesLeds = divideBy1023 * (ledsPerChannel);


Adafruit_NeoPixel leds = Adafruit_NeoPixel(numLeds, ledDataPin, NEO_GRB + NEO_KHZ800);

uint32_t black = leds.Color(0, 0, 0);

uint32_t colors[5] = {
  leds.Color(0, 150, 150),
  leds.Color(0, 255, 0),
  leds.Color(255, 255, 0),
  leds.Color(255, 105, 0),
  leds.Color(255, 0, 0)
};

int readPin = 0;
int strobePin = 2;
int resetPin = 3;

int spectrum[7] = {0, 0, 0, 0, 0, 0, 0};

void setup() {
  //#if defined DEBUG
  Serial.begin(2000000);
  //#endif
  
  pinMode(readPin, INPUT);
  pinMode(strobePin, OUTPUT);
  pinMode(resetPin, OUTPUT);

  digitalWrite(resetPin, LOW);
  digitalWrite(strobePin, HIGH);

  leds.begin();
  leds.setBrightness(30);
}

void loop() {
  #if defined DEBUG
  unsigned long start = micros();
  #endif

  readSpectrum();
  displayAll();

  #if defined DEBUG
  unsigned long endTime = micros();
  Serial.println(endTime - start);
  #endif
}

void readSpectrum() {
  digitalWrite(resetPin, HIGH);
  digitalWrite(resetPin, LOW);

  delayMicroseconds(75);
  
  for (int i = 0; i < 7; i++) {
    digitalWrite(strobePin, LOW);
    delayMicroseconds(40);
    spectrum[i] = analogRead(readPin);
    digitalWrite(strobePin, HIGH);
    delayMicroseconds(40);
  }
}

void displayAll() {
  leds.clear();
  for (int i = 0; i < 7; i++) {
    displaySpectrum(spectrum[i], i);
  }

  leds.show();
}

void displaySpectrum(int value, int channel) {
  
  int level = value * divideTimesLeds;
  
  if (value > 100) {
    level++;
  }

  int beginningIndex = ledsPerChannel * channel;
  int endingIndex = beginningIndex + level;

  int ledIndex = 0;
  for(int i = beginningIndex; i < endingIndex; i++){
    leds.setPixelColor(i, colors[ledIndex++]);
  }
}


